<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;


use AppBundle\Service\ApiManager;

class ApiController extends FOSRestController
{
    /**
    * @Rest\Get("/api/teams")
    */
    public function getTeamsAction(ApiManager $apiManager)
    {
        $teams = $apiManager->getAllTeams();
        if ($teams === null)
          return new View("No se han encontrado equipos.", Response::HTTP_NOT_FOUND);

        return $teams;
    }

    /**
    * @Rest\Get("/api/teams/{id}/players")
    */
    public function getPlayersAction(ApiManager $apiManager, $id)
    {
        $teams = $apiManager->getPlayersByTeam($id);
        if ($teams === null)
          return new View("No se han encontrado jugadores.", Response::HTTP_NOT_FOUND);

        return $teams;
    }

    /**
    * @Rest\Post("/api/players")
    */
    public function postAction(ApiManager $apiManager, Request $request)
    {
        $name = $request->get('name');
        $nickname = $request->get('nickname');
        $dorsal = $request->get('dorsal');
        $position = $request->get('position');
        $team_id = $request->get('team_id');

        if(empty($name) || empty($nickname) || empty($dorsal) || empty($position) || empty($team_id))
            return new View("Faltán campos", Response::HTTP_NOT_ACCEPTABLE);

        $player = $apiManager->insertPlayer($name, $nickname, $dorsal, $position, $team_id);

        return $player;
    }

    

}