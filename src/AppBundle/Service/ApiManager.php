<?php
namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Team;
use AppBundle\Entity\Player;
 
class ApiManager {

    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
 
    public function getAllTeams() 
    {
        $teams = $this->em->getRepository('AppBundle:Team')->findBy( array(), array('name' => 'ASC') );
        return $teams;
    }

    public function getPlayersByTeam($id) 
    {
        $players = $this->em->getRepository('AppBundle:Player')->findByTeam($id);
        return $players;
    }
    
    public function insertPlayer($name, $nickname, $dorsal, $position, $team_id)
    {
        $player = new Player;
        $player->setName($name);
        $player->setNickname($nickname);
        $player->setDorsal($dorsal);
        $player->setPosition($position);
        $player->setNickname($nickname);

        $team = $this->em->getRepository('AppBundle:Team')->find($team_id);
        $player->setTeam($team);

        $this->em->persist($player);
        $this->em->flush();
        return $player;
    }
     
}