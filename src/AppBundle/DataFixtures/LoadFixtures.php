<?php 

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Team;
use AppBundle\Entity\Player;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // crear jugadores y equipos
        for ($i = 1; $i < 21; $i++) {
            $team = new Team();
            $team->setName('Equipo  '.$i);
            $team->setStadium('Estadio  '.$i);
            $manager->persist($team);

            for ($j=1; $j < 24; $j++) { 
                $player = new Player();
                $player->setName('Nombre Apellidos  '.$j);
                $player->setNickname('Jugador'.$j.'Equipo'.$i);
                $player->setDorsal($j);
                $positions = array("Portero", "Defensa", "Centrocampista", "Delantero");
                $position = array_rand($positions);
                $player->setPosition($positions[$position]);
                $player->setTeam($team);
                $manager->persist($player);
            }
        }

        $manager->flush();
    }
}