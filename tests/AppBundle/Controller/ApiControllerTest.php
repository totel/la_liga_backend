<?php

namespace Tests\AppBundle\Controller;
require('vendor/autoload.php');

class ApiControllerTest extends \PHPUnit_Framework_TestCase
{
    protected $client;

    protected function setUp()
    {
        $this->client = new \GuzzleHttp\Client([
            'base_uri' => 'http://localhost:8000'
        ]);
    }

    public function testGetTeam()
    {
        $response = $this->client->get('/api/teams');
        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);
        
        $data = json_decode($response->getBody(), true);
        $this->assertArrayHasKey('name', $data[0]);
        $this->assertArrayHasKey('stadium', $data[0]);        
    
    }
}
